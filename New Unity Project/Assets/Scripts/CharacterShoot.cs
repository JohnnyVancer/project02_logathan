﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/// <summary>
/// @author Logan Jones
/// This script will allow the player to shoot using LMB.
/// </summary>
public class CharacterShoot : MonoBehaviour
{

    public GameObject Bullet;

    public float shootForce = 10f;

    public static int totalPistolBullets = 50;
    public static int numPistolBullets = totalPistolBullets;

    public static int totalMachineGunBullets = 150;
    public static int numMachineGunBullets = totalMachineGunBullets;

    public static int totalSniperBullets = 20;
    public static int numSniperBullets = totalSniperBullets;

    public static int weaponDamage;

    public Text bulletCount;
    private string bulletString;
    public Text gunType;



    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (numPistolBullets > 0)
            {
                // Set shootforce
                shootForce = 5;

                // Set Weapon Damage
                weaponDamage = 10;

                //	create a  ray from camera position in irection camera nwas facing
                Ray rayOrigin = new Ray(Camera.main.transform.position, Camera.main.transform.forward);

                // 	create Bullet
                GameObject tempBullet = Instantiate(Bullet, rayOrigin.origin, Quaternion.identity);

                // 	tell it to ignore collision with the plaer
                Physics.IgnoreCollision(tempBullet.GetComponent<Collider>(), GetComponent<Collider>());

                //	add impluse force in the direction the camera is facing with added force
                tempBullet.GetComponent<Rigidbody>().AddRelativeForce(rayOrigin.direction * shootForce, ForceMode.Impulse);

                numPistolBullets--;

                bulletString = numPistolBullets.ToString();

                ShowPistolAmmo(bulletString);

                gunType.text = "Pistol";
            }
        }
        if (Input.GetKeyDown(KeyCode.F1))
        {
            PistolGun(numPistolBullets);
        }
        if (Input.GetKeyDown(KeyCode.F2))
        {
            MachineGun(numMachineGunBullets);
        }
        if (Input.GetKeyDown(KeyCode.F3))
        {
            SniperGun(numSniperBullets);
        }
    }
    void ShowPistolAmmo(string toPut)
    {
        //Verify any non-null bulletCount, if so give error
        bulletCount.text = toPut + "/" + totalPistolBullets;


    }

    void ShowMachineGunAmmo(string toPut)
    {
        //Verify any non-null bulletCount, if so give error
        bulletCount.text = toPut + "/" + totalMachineGunBullets;
    }

    void ShowSniperAmmo(string toPut)
    {
        //Verify any non-null bulletCount, if so give error
        bulletCount.text = toPut + "/" + totalSniperBullets;
    }

    void PistolGun(int numBullets)
    {
        Debug.Log("This is a Pistol");
        if (Input.GetMouseButtonDown(0))
        {
            if (numBullets > 0)
            {
                // Set shootforce
                shootForce = 5;

                // Set Weapon Damage
                weaponDamage = 10;

                //	create a  ray from camera position in irection camera nwas facing
                Ray rayOrigin = new Ray(Camera.main.transform.position, Camera.main.transform.forward);

                // 	create Bullet
                GameObject tempBullet = Instantiate(Bullet, rayOrigin.origin, Quaternion.identity);

                // 	tell it to ignore collision with the plaer
                Physics.IgnoreCollision(tempBullet.GetComponent<Collider>(), GetComponent<Collider>());

                //	add impluse force in the direction the camera is facing with added force
                tempBullet.GetComponent<Rigidbody>().AddRelativeForce(rayOrigin.direction * shootForce, ForceMode.Impulse);

                numPistolBullets--;

                bulletString = numPistolBullets.ToString();

                ShowPistolAmmo(bulletString);

                gunType.text = "Pistol";
            }
        }
    }
    void MachineGun(int numBullets)
    {
        Debug.Log("This is a MachineGun");
        if (Input.GetMouseButtonDown(0))
        {
            if (numBullets > 0)
            {
                // Set shootforce
                shootForce = 5;

                // Set Weapon Damage
                weaponDamage = 20;

                //	create a  ray from camera position in irection camera nwas facing
                Ray rayOrigin = new Ray(Camera.main.transform.position, Camera.main.transform.forward);

                // 	create Bullet
                GameObject tempBullet = Instantiate(Bullet, rayOrigin.origin, Quaternion.identity);

                // 	tell it to ignore collision with the plaer
                Physics.IgnoreCollision(tempBullet.GetComponent<Collider>(), GetComponent<Collider>());

                //	add impluse force in the direction the camera is facing with added force
                tempBullet.GetComponent<Rigidbody>().AddRelativeForce(rayOrigin.direction * shootForce, ForceMode.Impulse);

                numMachineGunBullets--;

                bulletString = numMachineGunBullets.ToString();

                ShowMachineGunAmmo(bulletString);

                gunType.text = "MachineGun";
            }
        }
    }
    void SniperGun(int numBullets)
    {
        Debug.Log("This is a Sniper");

        if (Input.GetMouseButtonDown(0))
        {
            if (numBullets > 0)
            {

                // Set shootforce
                shootForce = 5;

                // Set Weapon Damage
                weaponDamage = 50;

                //	create a  ray from camera position in irection camera nwas facing
                Ray rayOrigin = new Ray(Camera.main.transform.position, Camera.main.transform.forward);

                // 	create Bullet
                GameObject tempBullet = Instantiate(Bullet, rayOrigin.origin, Quaternion.identity);

                // 	tell it to ignore collision with the plaer
                Physics.IgnoreCollision(tempBullet.GetComponent<Collider>(), GetComponent<Collider>());

                //	add impluse force in the direction the camera is facing with added force
                tempBullet.GetComponent<Rigidbody>().AddRelativeForce(rayOrigin.direction * shootForce, ForceMode.Impulse);

                numSniperBullets--;

                bulletString = numSniperBullets.ToString();

                ShowSniperAmmo(bulletString);

                gunType.text = "Sniper";
            }
        }
    }
}
