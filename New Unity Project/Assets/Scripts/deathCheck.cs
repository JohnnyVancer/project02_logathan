﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor.SceneManagement;

public class deathCheck : MonoBehaviour {

    public GameObject player;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.position = Vector3.MoveTowards(transform.position, player.transform.position, 4 * Time.deltaTime);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "bullet")
        {
            Destroy(this.gameObject);
            Debug.Log("Hit!");
        }

        if(collision.gameObject.tag == "Player")
        {
            Debug.Log("Game Over");
            EditorSceneManager.LoadScene(2);
        }
    }
}
