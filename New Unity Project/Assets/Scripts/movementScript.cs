﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movementScript : MonoBehaviour {

    public float speed;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
        if(Input.GetKey(KeyCode.W))
        {
            transform.Translate(0, 0, speed / 5);
        }
        //if (Input.GetKey(KeyCode.S))
        //{
        //    transform.Translate(0, 0, -speed / 5);
        //}
        if (Input.GetKey(KeyCode.A))
        {
            transform.Rotate(0, -speed * 2, 0);
        }
        if (Input.GetKey(KeyCode.D))
        {
            transform.Rotate(0, speed * 2, 0);
        }
        if(Input.GetKey(KeyCode.Space))
        {
            transform.Translate(0, speed / 15, 0);
        }

    }
}
